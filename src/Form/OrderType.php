<?php

/*
 * This file is part of the Symfony package.
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\Order;
use App\Form\Type\DateTimePickerType;
use App\Form\Type\InvoiceInputType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class OrderType extends AbstractType
{
    private $slugger;

    // Form types are services, so you can inject other services in them if needed
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // For the full reference of options defined by each form field type
        // see https://symfony.com/doc/current/reference/forms/types.html

        // By default, form fields include the 'required' attribute, which enables
        // the client-side form validation. This means that you can't test the
        // server-side validation errors from the browser. To temporarily disable
        // this validation, set the 'required' attribute to 'false':
        // $builder->add('title', null, ['required' => false, ...]);

        $builder
            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Quantity',
            ])
            ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Amount',
            ])
            ->add('vat', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Vat',
            ])
            ->add('description', TextareaType::class, [
                'help' => 'help.post_summary',
                'label' => 'Description',
            ])
            ->add('invoice', InvoiceInputType::class, [
                'required' => false,
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                /** @var Order */
                $order = $event->getData();
                $net = $order->getAmount() * $order->getQuantity();
                $tax = ($net * $order->getVat()) / 100 ;
                $order->setTotal($net + $tax);
                // var_dump($order->getInvoice());die;
                // if (null !== $orderTitle = $order->getTitle()) {
                //     $order->setSlug($this->slugger->slug($orderTitle)->lower());
                // }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
