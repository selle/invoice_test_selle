<?php

/*
 * This file is part of the Symfony package.
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\Entity\Invoice;

/**
 * Defines the custom form field type used to change user's password.
 *
 * @author Romain Monteil <monteil.romain@gmail.com>
 */
class InvoiceInputType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('invoiceNumber', NumberType::class, [
                // 'label' => 'Invoice number',
            ])
            ->add('customerId', NumberType::class, [
                'label' => 'Customer ID',
            ])
            ->add('invoiceDate', DateTimePickerType::class, [
                'label' => 'Date',
                'help' => 'Select a date',
            ])
        ;
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent &$event) {
            /** @var Order */
            $invoice = $event->getData();
            $newInvoice = new Invoice();
            // var_dump($invoice['invoiceNumber']);die;
            $newInvoice->setInvoiceDate(@$invoice['invoiceDate']);
            $newInvoice->setInvoiceNumber(@$invoice['invoiceNumber']);
            $newInvoice->setCustomerId(@$invoice['customerId']);
            $event->setData($newInvoice);
            // $net = $order->getAmount() * $order->getQuantity();
            // $tax = ($net * $order->getVat()) / 100 ;
            // $order->setTotal($net + $tax);
            // dump($invoice);die;
            // if (null !== $orderTitle = $order->getTitle()) {
            //     $order->setSlug($this->slugger->slug($orderTitle)->lower());
            // }
        });
    }
}
