<?php

/*
 * This file is part of the Symfony package.
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Order;
use App\Event\CommentCreatedEvent;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use App\Repository\InvoiceRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage invoice contents in the public part of the site.
 *
 * @Route("/invoice")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/", defaults={"page": "1", "_format"="html"}, methods="GET", name="invoice_index")
     * @Route("/rss.xml", defaults={"page": "1", "_format"="xml"}, methods="GET", name="invoice_rss")
     * @Route("/page/{page<[1-9]\d*>}", defaults={"_format"="html"}, methods="GET", name="invoice_index_paginated")
     * @Cache(smaxage="10")
     *
     * NOTE: For standard formats, Symfony will also automatically choose the best
     * Content-Type header for the response.
     * See https://symfony.com/doc/current/routing.html#special-parameters
     */
    public function index(Request $request, int $page, string $_format, OrderRepository $invoices, InvoiceRepository $tags): Response
    {
        $tag = null;
        if ($request->query->has('customerid')) {
            $tag = $tags->findOneBy(['name' => $request->query->get('customerid')]);
        }
        $latestOrders = $invoices->findLatest($page, $tag);

        // Every template name also has two extensions that specify the format and
        // engine for that template.
        // See https://symfony.com/doc/current/templates.html#template-naming
        return $this->render('invoice/index.'.$_format.'.twig', [
            'paginator' => $latestOrders,
            'tagName' => $tag ? $tag->getName() : null,
        ]);
    }

    /**
     * @Route("/invoices/{id}", methods="GET", name="show_invoice")
     *
     * NOTE: The $invoice controller argument is automatically injected by Symfony
     * after performing a database query looking for a Order with the 'slug'
     * value given in the route.
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function invoiceShow(Order $invoice): Response
    {
        // Symfony's 'dump()' function is an improved version of PHP's 'var_dump()' but
        // it's not available in the 'prod' environment to prevent leaking sensitive information.
        // It can be used both in PHP files and Twig templates, but it requires to
        // have enabled the DebugBundle. Uncomment the following line to see it in action:
        //
        // dump($invoice, $this->getUser(), new \DateTime());

        return $this->render('invoice/invoice_show.html.twig', ['invoice' => $invoice]);
    }

    /**
     * Creates a new Order entity.
     *
     * @Route("/new", methods="GET|POST", name="invoice_new")
     *
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $invoice = new Order();
        // $invoice->setAuthor($this->getUser());

        // See https://symfony.com/doc/current/form/multiple_buttons.html
        $form = $this->createForm(OrderType::class, $invoice)
            ->add('saveAndCreateNew', SubmitType::class);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/forms.html#processing-forms
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($invoice);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/controller.html#flash-messages
            $this->addFlash('success', 'invoice.created_successfully');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('admin_invoice_new');
            }

            return $this->redirectToRoute('admin_invoice_index');
        }

        return $this->render('invoice/new.html.twig', [
            'invoice' => $invoice,
            'form' => $form->createView(),
        ]);
    }
    

    /**
     * @Route("/search", methods="GET", name="invoice_search")
     */
    public function search(Request $request, OrderRepository $invoices): Response
    {
        $query = $request->query->get('q', '');
        $limit = $request->query->get('l', 10);

        if (!$request->isXmlHttpRequest()) {
            return $this->render('invoice/search.html.twig', ['query' => $query]);
        }

        $foundOrders = $invoices->findBySearchQuery($query, $limit);

        $results = [];
        foreach ($foundOrders as $invoice) {
            $results[] = [
                'customerid' => htmlspecialchars($invoice->getInvoice()->getInvoiceNumber(), \ENT_COMPAT | \ENT_HTML5),
                'date' => $invoice->getInvoice()->getInvoiceDate()->format('M d, Y'),
                'author' => htmlspecialchars($invoice->getInvoice()->getCustomerId(), \ENT_COMPAT | \ENT_HTML5),
                'url' => $this->generateUrl('show_invoice', ['id' => $invoice->getId()]),
            ];
        }

        return $this->json($results);
    }

    /**
     * Displays a form to edit an existing Order entity.
     *
     * @Route("/{id<\d+>}/edit", methods="GET|POST", name="edit_invoice")
     */
    public function edit(Request $request, Order $post, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(OrderType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('edit_invoice', ['id' => $post->getId()]);
        }

        return $this->render('invoice/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Order entity.
     *
     * @Route("/{id}/delete", methods="POST", name="invoice_delete")
     * @IsGranted("delete", subject="post")
     */
    public function delete(Request $request, Order $post, EntityManagerInterface $entityManager): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_post_index');
        }

        // Delete the tags associated with this blog post. This is done automatically
        // by Doctrine, except for SQLite (the database used in this application)
        // because foreign key support is not enabled by default in SQLite
        $post->getTags()->clear();

        $entityManager->remove($post);
        $entityManager->flush();

        $this->addFlash('success', 'post.deleted_successfully');

        return $this->redirectToRoute('admin_post_index');
    }
}
